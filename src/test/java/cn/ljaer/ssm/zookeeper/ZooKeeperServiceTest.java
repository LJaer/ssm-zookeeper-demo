package cn.ljaer.ssm.zookeeper;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ZooKeeperServiceTest {

    private ZooKeeperService zooKeeperService;

    @Before
    public void before() {
        try {
            zooKeeperService = new ZooKeeperService("127.0.0.1:2181");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getSubPaths() {
        try {
            List<String> paths = zooKeeperService.getSubPaths("/locks");
            for (String path : paths) {
                System.out.println("path:" + path);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void updatePathValue() throws Exception {
        zooKeeperService.updatePathValue("/test/path", "mypath!");
    }

    @Test
    public void createPath() throws Exception {
        zooKeeperService.createPath("/test/path", "test data");
    }

    @Test
    public void deletePath() throws Exception {
        zooKeeperService.deletePath("/test/path");
    }

    @Test
    public void getPathValue() {
        try {
            System.out.println(zooKeeperService.getPathValue("/test/path"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void distriguteLock() {
        final String path = "/lock_test";
        long startTime = System.currentTimeMillis();

        ExecutorService executorService = Executors.newFixedThreadPool(1000);


        for (int i =0;i<1000;i++){
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    final StopWatch stopWatch = new StopWatch();
                    stopWatch.start();
                    try {
                        zooKeeperService.distributeLock(path, 10000, new Runnable() {

                            @Override
                                public void run() {
                                        stopWatch.stop();
                                        System.out.println("执行任务---"+Thread.currentThread().getName()+"---，获得锁耗时:" + stopWatch.getTime());
                                        try {
                                        Thread.sleep(10);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        executorService.shutdown();
        boolean flag = true;
        while (flag){
            if (executorService.isTerminated()){
                flag = false;
                System.out.println("执行耗时："+(System.currentTimeMillis()-startTime));
            }
        }
    }
}
