###参考链接

![基于zookeeper简单实现分布式锁](http://blog.csdn.net/desilting/article/details/41280869)

# spring4_springmvc_mybatis
环境：
win10
tomcat7
jdk7
spring4
##maven搭建的spring4+springmvc+mybatis框架

并进行简单测试
创建test数据库并导入sql文件夹下的sql语句建表

在浏览器输入http://localhost:8080/selectByPrimaryKey?id=1

可得到：User{id=1, username='zk', password='123'}

## zookeeper 分布式锁


两种方式，方案一，并发大时，请求会超时

```
执行任务---Thread[pool-3-thread-34,5,main]---，获得锁耗时:1049
ERROR [pool-3-thread-83] - 任务执行失败，在时间：1000ms内，未获得分布式锁!
ERROR [pool-3-thread-83] - 执行分布式锁任务异常。
java.lang.Exception: 任务执行失败，在时间：1000ms内，未获得分布式锁!
	at cn.ljaer.ssm.zookeeper.ZooKeeperService.distributeLock(ZooKeeperService.java:76)
	at cn.ljaer.ssm.zookeeper.ZooKeeperServiceTest$1.run(ZooKeeperServiceTest.java:75)
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at java.lang.Thread.run(Thread.java:745)
java.lang.Exception: 执行分布式锁任务异常。
	at cn.ljaer.ssm.zookeeper.ZooKeeperService.distributeLock(ZooKeeperService.java:80)
	at cn.ljaer.ssm.zookeeper.ZooKeeperServiceTest$1.run(ZooKeeperServiceTest.java:75)
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at java.lang.Thread.run(Thread.java:745)
```

### 方案一：

对同一路径下文件获取锁，谁先锁定，先执行谁

### 方案二：

当很多进程需要访问共享资源时，我们可以通过zk来实现分布式锁。主要步骤是：
* 1.建立一个节点，假如名为：lock 。节点类型为持久节点（PERSISTENT）
* 2.每当进程需要访问共享资源时，会调用分布式锁的lock()或tryLock()方法获得锁，这个时候会在第一步创建的lock节点下建立相应的顺序子节点，节点类型为临时顺序节点（EPHEMERAL_SEQUENTIAL），通过组成特定的名字name+lock+顺序号。
* 3.在建立子节点后，对lock下面的所有以name开头的子节点进行排序，判断刚刚建立的子节点顺序号是否是最小的节点，假如是最小节点，则获得该锁对资源进行访问。
* 4.假如不是该节点，就获得该节点的上一顺序节点，并给该节点是否存在注册监听事件。同时在这里阻塞。等待监听事件的发生，获得锁控制权。
* 5.当调用完共享资源后，调用unlock（）方法，关闭zk，进而可以引发监听事件，释放该锁。
实现的分布式锁是严格的按照顺序访问的并发锁。

## 测试

### 1、ZooKeeperPropertyPlaceholderConfigurerTest 

测试 zookeeper 配置文件

### 2、ZookeeperWatchServiceTest


ZookeeperWatchServiceTest.addNodeWatch() 监听节点变化测试

输出结果：

```
获取到数据Loaded-1/summall/conf/jdbc.username：mypath.......9
获取到数据Loaded-2/summall/conf/jdbc.username：mypath.......9
获取到数据变化-1/summall/conf/jdbc.username：mypath.......0
获取到数据变化-1/summall/conf/jdbc.username：mypath.......1
获取到数据变化-1/summall/conf/jdbc.username：mypath.......2
获取到数据变化-1/summall/conf/jdbc.username：mypath.......3
获取到数据变化-1/summall/conf/jdbc.username：mypath.......4
获取到数据变化-1/summall/conf/jdbc.username：mypath.......5
获取到数据变化-1/summall/conf/jdbc.username：mypath.......6
获取到数据变化-1/summall/conf/jdbc.username：mypath.......7
获取到数据变化-1/summall/conf/jdbc.username：mypath.......8
获取到数据变化-1/summall/conf/jdbc.username：mypath.......9
```

ZookeeperWatchServiceTest.setPathValue() 更新节点

### 3、ZooKeeperServiceTest

分布式锁第一种方式测试

ZooKeeperServiceTest.distriguteLock()

```
 INFO [main-EventThread] - State change: CONNECTED
执行任务---Thread[pool-3-thread-5,5,main]---，获得锁耗时:250
执行任务---Thread[pool-3-thread-52,5,main]---，获得锁耗时:368
执行任务---Thread[pool-3-thread-48,5,main]---，获得锁耗时:397
执行任务---Thread[pool-3-thread-56,5,main]---，获得锁耗时:435
执行任务---Thread[pool-3-thread-57,5,main]---，获得锁耗时:452
执行任务---Thread[pool-3-thread-60,5,main]---，获得锁耗时:594
执行任务---Thread[pool-3-thread-53,5,main]---，获得锁耗时:683
执行任务---Thread[pool-3-thread-64,5,main]---，获得锁耗时:875
执行任务---Thread[pool-3-thread-44,5,main]---，获得锁耗时:925
执行任务---Thread[pool-3-thread-68,5,main]---，获得锁耗时:949
执行任务---Thread[pool-3-thread-40,5,main]---，获得锁耗时:972
执行任务---Thread[pool-3-thread-72,5,main]---，获得锁耗时:991
执行任务---Thread[pool-3-thread-61,5,main]---，获得锁耗时:971
执行任务---Thread[pool-3-thread-28,5,main]---，获得锁耗时:1034
执行任务---Thread[pool-3-thread-76,5,main]---，获得锁耗时:1053
执行任务---Thread[pool-3-thread-36,5,main]---，获得锁耗时:1076
执行任务---Thread[pool-3-thread-80,5,main]---，获得锁耗时:1095
执行任务---Thread[pool-3-thread-84,5,main]---，获得锁耗时:1116
执行任务---Thread[pool-3-thread-32,5,main]---，获得锁耗时:1139
执行任务---Thread[pool-3-thread-11,5,main]---，获得锁耗时:1166
执行任务---Thread[pool-3-thread-65,5,main]---，获得锁耗时:1137
执行任务---Thread[pool-3-thread-96,5,main]---，获得锁耗时:1202
执行任务---Thread[pool-3-thread-92,5,main]---，获得锁耗时:1223
执行任务---Thread[pool-3-thread-21,5,main]---，获得锁耗时:1233
执行任务---Thread[pool-3-thread-49,5,main]---，获得锁耗时:1227
执行任务---Thread[pool-3-thread-88,5,main]---，获得锁耗时:1285
执行任务---Thread[pool-3-thread-100,5,main]---，获得锁耗时:1306
执行任务---Thread[pool-3-thread-24,5,main]---，获得锁耗时:1329
执行任务---Thread[pool-3-thread-22,5,main]---，获得锁耗时:1349
执行任务---Thread[pool-3-thread-26,5,main]---，获得锁耗时:1369
执行任务---Thread[pool-3-thread-69,5,main]---，获得锁耗时:1349
执行任务---Thread[pool-3-thread-25,5,main]---，获得锁耗时:1401
执行任务---Thread[pool-3-thread-3,5,main]---，获得锁耗时:1444
执行任务---Thread[pool-3-thread-18,5,main]---，获得锁耗时:1465
执行任务---Thread[pool-3-thread-19,5,main]---，获得锁耗时:1486
执行任务---Thread[pool-3-thread-13,5,main]---，获得锁耗时:1512
执行任务---Thread[pool-3-thread-29,5,main]---，获得锁耗时:1505
执行任务---Thread[pool-3-thread-98,5,main]---，获得锁耗时:1527
执行任务---Thread[pool-3-thread-94,5,main]---，获得锁耗时:1548
执行任务---Thread[pool-3-thread-73,5,main]---，获得锁耗时:1540
执行任务---Thread[pool-3-thread-90,5,main]---，获得锁耗时:1598
执行任务---Thread[pool-3-thread-45,5,main]---，获得锁耗时:1590
执行任务---Thread[pool-3-thread-20,5,main]---，获得锁耗时:1663
执行任务---Thread[pool-3-thread-33,5,main]---，获得锁耗时:1661
执行任务---Thread[pool-3-thread-7,5,main]---，获得锁耗时:1704
执行任务---Thread[pool-3-thread-86,5,main]---，获得锁耗时:1702
执行任务---Thread[pool-3-thread-77,5,main]---，获得锁耗时:1692
执行任务---Thread[pool-3-thread-17,5,main]---，获得锁耗时:1766
执行任务---Thread[pool-3-thread-82,5,main]---，获得锁耗时:1766
执行任务---Thread[pool-3-thread-78,5,main]---，获得锁耗时:1795
执行任务---Thread[pool-3-thread-10,5,main]---，获得锁耗时:1932
执行任务---Thread[pool-3-thread-15,5,main]---，获得锁耗时:2039
执行任务---Thread[pool-3-thread-9,5,main]---，获得锁耗时:2162
执行任务---Thread[pool-3-thread-37,5,main]---，获得锁耗时:2186
执行任务---Thread[pool-3-thread-41,5,main]---，获得锁耗时:2179
执行任务---Thread[pool-3-thread-74,5,main]---，获得锁耗时:2229
执行任务---Thread[pool-3-thread-70,5,main]---，获得锁耗时:2250
执行任务---Thread[pool-3-thread-66,5,main]---，获得锁耗时:2272
执行任务---Thread[pool-3-thread-62,5,main]---，获得锁耗时:2292
执行任务---Thread[pool-3-thread-58,5,main]---，获得锁耗时:2312
执行任务---Thread[pool-3-thread-54,5,main]---，获得锁耗时:2334
执行任务---Thread[pool-3-thread-50,5,main]---，获得锁耗时:2354
执行任务---Thread[pool-3-thread-46,5,main]---，获得锁耗时:2379
执行任务---Thread[pool-3-thread-42,5,main]---，获得锁耗时:2401
执行任务---Thread[pool-3-thread-30,5,main]---，获得锁耗时:2425
执行任务---Thread[pool-3-thread-81,5,main]---，获得锁耗时:2407
执行任务---Thread[pool-3-thread-16,5,main]---，获得锁耗时:2493
执行任务---Thread[pool-3-thread-2,5,main]---，获得锁耗时:2520
执行任务---Thread[pool-3-thread-8,5,main]---，获得锁耗时:2536
执行任务---Thread[pool-3-thread-85,5,main]---，获得锁耗时:2555
执行任务---Thread[pool-3-thread-89,5,main]---，获得锁耗时:2575
执行任务---Thread[pool-3-thread-93,5,main]---，获得锁耗时:2597
执行任务---Thread[pool-3-thread-97,5,main]---，获得锁耗时:2617
执行任务---Thread[pool-3-thread-14,5,main]---，获得锁耗时:2698
执行任务---Thread[pool-3-thread-4,5,main]---，获得锁耗时:2722
执行任务---Thread[pool-3-thread-6,5,main]---，获得锁耗时:2791
执行任务---Thread[pool-3-thread-1,5,main]---，获得锁耗时:2825
执行任务---Thread[pool-3-thread-34,5,main]---，获得锁耗时:2821
执行任务---Thread[pool-3-thread-38,5,main]---，获得锁耗时:2841
执行任务---Thread[pool-3-thread-12,5,main]---，获得锁耗时:2881
执行任务---Thread[pool-3-thread-23,5,main]---，获得锁耗时:2840
执行任务---Thread[pool-3-thread-27,5,main]---，获得锁耗时:2860
执行任务---Thread[pool-3-thread-31,5,main]---，获得锁耗时:2881
执行任务---Thread[pool-3-thread-35,5,main]---，获得锁耗时:2890
执行任务---Thread[pool-3-thread-39,5,main]---，获得锁耗时:2911
执行任务---Thread[pool-3-thread-43,5,main]---，获得锁耗时:2932
执行任务---Thread[pool-3-thread-47,5,main]---，获得锁耗时:2951
执行任务---Thread[pool-3-thread-51,5,main]---，获得锁耗时:2973
执行任务---Thread[pool-3-thread-55,5,main]---，获得锁耗时:3009
执行任务---Thread[pool-3-thread-59,5,main]---，获得锁耗时:3036
执行任务---Thread[pool-3-thread-63,5,main]---，获得锁耗时:3064
执行任务---Thread[pool-3-thread-67,5,main]---，获得锁耗时:3090
执行任务---Thread[pool-3-thread-71,5,main]---，获得锁耗时:3110
执行任务---Thread[pool-3-thread-75,5,main]---，获得锁耗时:3132
执行任务---Thread[pool-3-thread-79,5,main]---，获得锁耗时:3152
执行任务---Thread[pool-3-thread-83,5,main]---，获得锁耗时:3172
执行任务---Thread[pool-3-thread-87,5,main]---，获得锁耗时:3194
执行任务---Thread[pool-3-thread-91,5,main]---，获得锁耗时:3214
执行任务---Thread[pool-3-thread-95,5,main]---，获得锁耗时:3237
执行任务---Thread[pool-3-thread-99,5,main]---，获得锁耗时:3256
执行耗时：3367
```

### 4、DistributedLock2

分布式锁第二种方式测试

DistributedLock2.main

```
 INFO [Thread-3] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@446b75a6
 INFO [Thread-6] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@79369be0
 INFO [Thread-7] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@1225d293
 INFO [Thread-1] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@2f1e482a
 INFO [Thread-5] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@18dde517
 INFO [Thread-9] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@6395c130
 INFO [Thread-0] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@76a8b2dc
 INFO [Thread-4] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@6fd0728b
 INFO [Thread-8] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@43b513c9
 INFO [Thread-2] - Initiating client connection, connectString=127.0.0.1:2181 sessionTimeout=10000 watcher=cn.ljaer.ssm.zookeeper.DistributedLock2@5b9a20e4
 INFO [Thread-5-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-7-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-9-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-7-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-5-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-9-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-0-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-2-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-2-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-6-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-6-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-3-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-8-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-3-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-4-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-4-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-1-SendThread(127.0.0.1:2181)] - Opening socket connection to server 127.0.0.1/127.0.0.1:2181. Will not attempt to authenticate using SASL (unknown error)
 INFO [Thread-1-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-8-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-0-SendThread(127.0.0.1:2181)] - Socket connection established to 127.0.0.1/127.0.0.1:2181, initiating session
 INFO [Thread-7-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b0, negotiated timeout = 10000
 INFO [Thread-5-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b2, negotiated timeout = 10000
 INFO [Thread-9-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b1, negotiated timeout = 10000
 INFO [Thread-5-EventThread] - 【第6个线程】成功连接上ZK服务器
 INFO [Thread-7-EventThread] - 【第8个线程】成功连接上ZK服务器
 INFO [Thread-9-EventThread] - 【第10个线程】成功连接上ZK服务器
 INFO [Thread-2-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b3, negotiated timeout = 10000
 INFO [Thread-2-EventThread] - 【第3个线程】成功连接上ZK服务器
 INFO [Thread-6-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b4, negotiated timeout = 10000
 INFO [Thread-6-EventThread] - 【第7个线程】成功连接上ZK服务器
 INFO [Thread-3-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b5, negotiated timeout = 10000
 INFO [Thread-3-EventThread] - 【第4个线程】成功连接上ZK服务器
 INFO [Thread-4-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b6, negotiated timeout = 10000
 INFO [Thread-4-EventThread] - 【第5个线程】成功连接上ZK服务器
 INFO [Thread-1-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b7, negotiated timeout = 10000
 INFO [Thread-1-EventThread] - 【第2个线程】成功连接上ZK服务器
 INFO [Thread-8-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b8, negotiated timeout = 10000
 INFO [Thread-8-EventThread] - 【第9个线程】成功连接上ZK服务器
 INFO [Thread-0-SendThread(127.0.0.1:2181)] - Session establishment complete on server 127.0.0.1/127.0.0.1:2181, sessionid = 0x15f9ad1eacb00b9, negotiated timeout = 10000
 INFO [Thread-0-EventThread] - 【第1个线程】成功连接上ZK服务器
 INFO [Thread-8] - 【第9个线程】创建锁路径:/disLocks/sub0000000070
 INFO [Thread-3] - 【第4个线程】创建锁路径:/disLocks/sub0000000074
 INFO [Thread-1] - 【第2个线程】创建锁路径:/disLocks/sub0000000073
 INFO [Thread-4] - 【第5个线程】创建锁路径:/disLocks/sub0000000071
 INFO [Thread-0] - 【第1个线程】创建锁路径:/disLocks/sub0000000072
 INFO [Thread-5] - 【第6个线程】创建锁路径:/disLocks/sub0000000075
 INFO [Thread-6] - 【第7个线程】创建锁路径:/disLocks/sub0000000076
 INFO [Thread-8] - 【第9个线程】子节点中，我果然是老大/disLocks/sub0000000070
 INFO [Thread-6] - 【第7个线程】获取子节点中，排在我前面的/disLocks/sub0000000075
 INFO [Thread-4] - 【第5个线程】获取子节点中，排在我前面的/disLocks/sub0000000070
 INFO [Thread-3] - 【第4个线程】获取子节点中，排在我前面的/disLocks/sub0000000073
 INFO [Thread-0] - 【第1个线程】获取子节点中，排在我前面的/disLocks/sub0000000071
 INFO [Thread-1] - 【第2个线程】获取子节点中，排在我前面的/disLocks/sub0000000072
 INFO [Thread-5] - 【第6个线程】获取子节点中，排在我前面的/disLocks/sub0000000074
 INFO [Thread-8] - 【第9个线程】获取锁成功，赶紧干活！
 INFO [Thread-2] - 【第3个线程】创建锁路径:/disLocks/sub0000000077
 INFO [Thread-9] - 【第10个线程】创建锁路径:/disLocks/sub0000000078
 INFO [Thread-2] - 【第3个线程】获取子节点中，排在我前面的/disLocks/sub0000000076
 INFO [Thread-9] - 【第10个线程】获取子节点中，排在我前面的/disLocks/sub0000000077
 INFO [Thread-7] - 【第8个线程】创建锁路径:/disLocks/sub0000000079
 INFO [Thread-7] - 【第8个线程】获取子节点中，排在我前面的/disLocks/sub0000000078
 INFO [Thread-8] - 【第9个线程】删除本节点：/disLocks/sub0000000070
 INFO [Thread-4-EventThread] - 【第5个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-8] - Session: 0x15f9ad1eacb00b8 closed
 INFO [Thread-8] - 【第9个线程】释放连接
 INFO [Thread-4-EventThread] - 【第5个线程】子节点中，我果然是老大/disLocks/sub0000000071
 INFO [Thread-8-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b8
 INFO [Thread-4-EventThread] - 【第5个线程】获取锁成功，赶紧干活！
 INFO [Thread-4-EventThread] - 【第5个线程】删除本节点：/disLocks/sub0000000071
 INFO [Thread-0-EventThread] - 【第1个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-0-EventThread] - 【第1个线程】子节点中，我果然是老大/disLocks/sub0000000072
 INFO [Thread-4-EventThread] - Session: 0x15f9ad1eacb00b6 closed
 INFO [Thread-4-EventThread] - 【第5个线程】释放连接
 INFO [Thread-4-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b6
 INFO [Thread-0-EventThread] - 【第1个线程】获取锁成功，赶紧干活！
 INFO [Thread-0-EventThread] - 【第1个线程】删除本节点：/disLocks/sub0000000072
 INFO [Thread-1-EventThread] - 【第2个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-0-EventThread] - Session: 0x15f9ad1eacb00b9 closed
 INFO [Thread-0-EventThread] - 【第1个线程】释放连接
 INFO [Thread-1-EventThread] - 【第2个线程】子节点中，我果然是老大/disLocks/sub0000000073
 INFO [Thread-0-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b9
 INFO [Thread-1-EventThread] - 【第2个线程】获取锁成功，赶紧干活！
 INFO [Thread-1-EventThread] - 【第2个线程】删除本节点：/disLocks/sub0000000073
 INFO [Thread-3-EventThread] - 【第4个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-3-EventThread] - 【第4个线程】子节点中，我果然是老大/disLocks/sub0000000074
 INFO [Thread-1-EventThread] - Session: 0x15f9ad1eacb00b7 closed
 INFO [Thread-3-EventThread] - 【第4个线程】获取锁成功，赶紧干活！
 INFO [Thread-1-EventThread] - 【第2个线程】释放连接
 INFO [Thread-1-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b7
 INFO [Thread-3-EventThread] - 【第4个线程】删除本节点：/disLocks/sub0000000074
 INFO [Thread-5-EventThread] - 【第6个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-5-EventThread] - 【第6个线程】子节点中，我果然是老大/disLocks/sub0000000075
 INFO [Thread-3-EventThread] - Session: 0x15f9ad1eacb00b5 closed
 INFO [Thread-3-EventThread] - 【第4个线程】释放连接
 INFO [Thread-3-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b5
 INFO [Thread-5-EventThread] - 【第6个线程】获取锁成功，赶紧干活！
 INFO [Thread-5-EventThread] - 【第6个线程】删除本节点：/disLocks/sub0000000075
 INFO [Thread-6-EventThread] - 【第7个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-6-EventThread] - 【第7个线程】子节点中，我果然是老大/disLocks/sub0000000076
 INFO [Thread-5-EventThread] - Session: 0x15f9ad1eacb00b2 closed
 INFO [Thread-5-EventThread] - 【第6个线程】释放连接
 INFO [Thread-5-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b2
 INFO [Thread-6-EventThread] - 【第7个线程】获取锁成功，赶紧干活！
 INFO [Thread-6-EventThread] - 【第7个线程】删除本节点：/disLocks/sub0000000076
 INFO [Thread-2-EventThread] - 【第3个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-6-EventThread] - Session: 0x15f9ad1eacb00b4 closed
 INFO [Thread-6-EventThread] - 【第7个线程】释放连接
 INFO [Thread-6-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b4
 INFO [Thread-2-EventThread] - 【第3个线程】子节点中，我果然是老大/disLocks/sub0000000077
 INFO [Thread-2-EventThread] - 【第3个线程】获取锁成功，赶紧干活！
 INFO [Thread-2-EventThread] - 【第3个线程】删除本节点：/disLocks/sub0000000077
 INFO [Thread-9-EventThread] - 【第10个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-2-EventThread] - Session: 0x15f9ad1eacb00b3 closed
 INFO [Thread-2-EventThread] - 【第3个线程】释放连接
 INFO [Thread-2-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b3
 INFO [Thread-9-EventThread] - 【第10个线程】子节点中，我果然是老大/disLocks/sub0000000078
 INFO [Thread-9-EventThread] - 【第10个线程】获取锁成功，赶紧干活！
 INFO [Thread-9-EventThread] - 【第10个线程】删除本节点：/disLocks/sub0000000078
 INFO [Thread-7-EventThread] - 【第8个线程】收到情报，排我前面的家伙已挂，我是不是可以出山了？
 INFO [Thread-7-EventThread] - 【第8个线程】子节点中，我果然是老大/disLocks/sub0000000079
 INFO [Thread-9-EventThread] - Session: 0x15f9ad1eacb00b1 closed
 INFO [Thread-9-EventThread] - 【第10个线程】释放连接
 INFO [Thread-9-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b1
 INFO [Thread-7-EventThread] - 【第8个线程】获取锁成功，赶紧干活！
 INFO [Thread-7-EventThread] - 【第8个线程】删除本节点：/disLocks/sub0000000079
 INFO [Thread-7-EventThread] - Session: 0x15f9ad1eacb00b0 closed
 INFO [Thread-7-EventThread] - 【第8个线程】释放连接
 INFO [Thread-7-EventThread] - EventThread shut down for session: 0x15f9ad1eacb00b0
 INFO [main] - 所有线程运行结束!

Process finished with exit code 0

```



